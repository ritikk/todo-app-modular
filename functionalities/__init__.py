from flask import Flask, render_template, request, redirect

app = Flask(__name__)

from functionalities import routes
