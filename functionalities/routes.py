from functionalities import app

from flask import render_template, request, redirect

from models import Todo, db


@app.route("/", methods=["GET", "POST"])
def hello_world():
    if request.method == 'POST':
        title = (request.form['title'])
        desc = (request.form['desc'])

        todo = Todo(title=title, desc=desc)
        db.session.add(todo)
        db.session.commit()

    allTodo = Todo.query.all()
    print(allTodo)
    # return "<p>Hello, world!</p>"
    return render_template("index.html", allTodo=allTodo)


@app.route("/show")
def show():
    allTodo = Todo.query.all()
    print(allTodo)
    return "This is my product page"


@app.route("/update/<int:sno>", methods=["GET", "POST"])
def update(sno):
    if request.method == 'POST':
        title = (request.form['title'])
        desc = (request.form['desc'])

        todo = Todo.query.filter_by(sno=sno).first()
        todo.title = title
        todo.desc = desc
        db.session.add(todo)
        db.session.commit()
        return redirect("/")

    todo = Todo.query.filter_by(sno=sno).first()

    return render_template("update.html", todo=todo)


@app.route("/delete/<int:sno>")
def delete(sno):
    todo = Todo.query.filter_by(sno=sno).first()
    db.session.delete(todo)
    db.session.commit()
    return redirect("/")
